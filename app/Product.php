<?php namespace LojaVirtual;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['name', 'description', 'price', 'featured', 'recommended', 'category_id', 'tag_id'];


    public function category()
    {
        return $this->belongsTo('LojaVirtual\Category');
    }


    public function images()
    {
        return $this->hasMany('LojaVirtual\ProductImages');
    }


    public function tags()
    {
        return $this->belongsToMany('LojaVirtual\Tag');
    }

    public function getTagListAttribute()
    {
        $tags = $this->tags->lists('name')->toArray();
        return implode(",", $tags);
    }


    public function scopeFeatured($query)
    {
        return $query->where('featured', '=', 1);
    }

    public function scopeRecommended($query)
    {
        return $query->where('recommended', '=', 1);
    }

    public function scopeOfCategory($query, $type)
    {
        return $query->where('category_id', '=', $type);
    }



    protected static function boot()
    {
        parent::boot();
        static::deleted(function ($prod) {
            $prod->images()->delete();
        });
    }

}
