<?php

namespace LojaVirtual\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LojaVirtual\Http\Requests;
use LojaVirtual\Http\Controllers\Controller;
use LojaVirtual\User;

class ClientController extends Controller
{

    public function index()
    {
        $orders = Auth::user()->orders;
        return view('store.client-index', compact('orders'));
    }


    public function create()
    {
        return view('store.client-create');
    }


    public function store(Requests\ClientRequest $clientRequest)
    {
        User::create([
            'name' => $clientRequest->get('name'),
            'email' => $clientRequest->get('email'),
            'password' => bcrypt($clientRequest->get('password')),
            ]);
        return redirect()->route('login.client');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

}
