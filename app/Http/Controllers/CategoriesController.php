<?php namespace LojaVirtual\Http\Controllers;

use LojaVirtual\Category;
use LojaVirtual\Http\Requests;

class CategoriesController extends Controller
{

    private $categoryModel;

    public function __construct(Category $categoryModel)
    {
        return $this->categoryModel = $categoryModel;
    }


    /**
     * @return All lists from @categoryModel
     */
    public function index()
    {
        $categories = $this->categoryModel->paginate(10);
        return view('admin.categories.index', compact('categories'));
    }


    /**
     * @return Show form to create a new category in resource
     */
    public function create()
    {
        return view('admin.categories.create');
    }


    /**
     * Save a new request from category input form
     * @param Requests\CategoryRequest $request
     * @return Create a new data in Category table and redirect to index categories page
     */
    public function store(Requests\CategoryRequest $request)
    {
        $input = $request->all();
        $category = $this->categoryModel->fill($input);
        $category->save();
        return redirect()->route('categories');
    }


    /**
     * @param $id
     * @return Get $id data and return a form to edit values
     */
    public function edit($id)
    {
        $category = $this->categoryModel->find($id);
        return view('admin.categories.edit', compact('category'));
    }


    /**
     * @param Requests\CategoryRequest $request
     * @param $id
     * @return Get values from category, update and redirect to index categories list
     */
    public function update(Requests\CategoryRequest $request, $id)
    {
        $this->categoryModel->find($id)->update($request->all());
        return redirect()->route('categories');
    }


    /**
     * @param $id
     * @return Delete data and redirect for the list categories page
     * @throws \Exception
     */
    public function destroy($id)
    {
        $this->categoryModel->find($id)->delete();
        return redirect()->route('categories');
    }

}
