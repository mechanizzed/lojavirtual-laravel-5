<?php namespace LojaVirtual\Http\Controllers;

use LojaVirtual\Category;
use LojaVirtual\Http\Requests;
use LojaVirtual\Product;
use LojaVirtual\Tag;

class StoreController extends Controller
{


    /**
     * @return Index page from the store
     */
    public function index()
    {
        $pFeatured = Product::featured()->get();
        $pRecommended = Product::recommended()->get();
        $categories = Category::all();
        return view('store.index', compact('categories', 'pFeatured', 'pRecommended'));
    }


    public function category($id)
    {
        $categories = Category::all();
        $category = Category::find($id);
        $products = Product::ofCategory($id)->get();
        return view('store.category', compact('categories', 'category', 'products'));
    }

    public function tag($id)
    {
        $categories = Category::all();
        $tag = Tag::find($id);
        $products = $tag->products;
        return view('store.tags', compact('categories', 'tag', 'products'));
    }


    public function product($id)
    {
        $categories = Category::all();
        $product = Product::find($id);
        return view('store.product-show', compact('categories', 'product'));
    }

}
