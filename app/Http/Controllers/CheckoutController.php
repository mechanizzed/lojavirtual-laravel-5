<?php

namespace LojaVirtual\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use LojaVirtual\Http\Requests;
use LojaVirtual\Order;
use LojaVirtual\OrderItem;
use PHPSC\PagSeguro\Items\Item;
use PHPSC\PagSeguro\Requests\Checkout\CheckoutService;

class CheckoutController extends Controller
{


	public function place(Order $orderModel, OrderItem $orderItem, CheckoutService $checkoutService)
	{
		if ( ! Session::has('cart')) {
			return redirect()->route('cart');
		}


		$cart = Session::get('cart');
		if ($cart->getTotal() > 0) {

			$checkout = $checkoutService->createCheckoutBuilder();
			
			$order = $orderModel->create([
				'user_id' => Auth::user()->id,
				'total'   => $cart->getTotal()
				]);

			foreach ($cart->all() as $k => $item) {
				$checkout->addItem(new Item($k, $item['name'], $item['price'], $item['qtd']));
				$order->items()->create(['product_id' => $k,'price' => $item['price'],'qtd' => $item['qtd']]);
			}
			$cart->clear();
			$checkout->getCheckout();
			$response = $checkoutService->checkout($checkout->getCheckout());
			return redirect($response->getRedirectionUrl());
			//return view('store.checkout',compact('order'));
		}

		return redirect()->route('cart');
	}


	public function test(CheckoutService $checkoutService)
	{

		$checkout = $checkoutService->createCheckoutBuilder()
		->addItem(new Item(1, 'Televisão LED 500', 8999.99))
		->addItem(new Item(2, 'Video-game mega ultra blaster', 799.99))
		->getCheckout();

		$response = $checkoutService->checkout($checkout);

		return redirect($response->getRedirectionUrl());

	}


}
