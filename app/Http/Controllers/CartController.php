<?php namespace LojaVirtual\Http\Controllers;

use Illuminate\Support\Facades\Session;
use LojaVirtual\Cart;
use LojaVirtual\Http\Requests;
use LojaVirtual\Product;

class CartController extends Controller
{

    /**
     * @var Cart
     */
    private $cart;

    /**
     * @param Cart $cart
     */
    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }


    public function index()
    {
        if ( ! Session::has('cart')) {
            Session::set('cart', $this->cart);
        }

        return view('store.cart', ['cart' => Session::get('cart')]);
    }


    public function add($id)
    {
        $cart = $this->getCart();

        $product = Product::find($id);
        $image = $product->images()->first();
        if ($image == null) {
            $image = null;
        } else {
            $image = $image->file;
        }
        $cart->add($id, $product->name, $product->price, $image);
        Session::set('cart', $cart);

        return redirect()->route('cart');
    }


    public function remove($id, $qtd)
    {
        if ($qtd <= 1) {
            return $this->destroy($id);
        }

        $cart = $this->getCart();

        $product = Product::find($id);
        $image = $product->images()->first();
        if ($image == null) {
            $image = null;
        } else {
            $image = $image->file;
        }
        $cart->remove($id, $product->name, $product->price, $image);
        Session::set('cart', $cart);
        return redirect()->route('cart');
    }

    public function destroy($id)
    {
        $cart = $this->getCart();
        $cart->destroy($id);
        Session::set('cart', $cart);

        return redirect()->route('cart');
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        if (Session::has('cart')) {
            $cart = Session::get('cart');
        } else {
            $cart = $this->cart;
        }

        return $cart;
    }


}
