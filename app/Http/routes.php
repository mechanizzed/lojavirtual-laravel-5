<?php

/*
|--------------------------------------------------------------------------
| Application Routes from store (front)
|--------------------------------------------------------------------------
*/


/**
 * Routes from store page (front)
 */
Route::get('/', 'StoreController@index');
Route::get('category/{id}', ['as' => 'category', 'uses' => 'StoreController@category']);
Route::get('tag/{id}', ['as' => 'tag', 'uses' => 'StoreController@tag']);
Route::get('product/{id}', ['as' => 'product', 'uses' => 'StoreController@product']);
Route::get('cart', ['as' => 'cart', 'uses' => 'CartController@index']);
Route::get('cart/add/{id}', ['as' => 'cart.add', 'uses' => 'CartController@add']);
Route::get('cart/destroy/{id}', ['as' => 'cart.destroy', 'uses' => 'CartController@destroy']);
Route::get('cart/remove/{id}/{qtd}', ['as' => 'cart.remove', 'uses' => 'CartController@remove']);


/**
 * Routes from checkout place order
 */
Route::get('checkout/placeOrder', ['as' => 'checkout.place', 'middleware' => 'auth.client', 'uses' => 'CheckoutController@place']);

/**
 * LOGIN PAGE CLIENT
 */
Route::get('login', ['as' => 'login.client', 'uses' => 'LoginClientController@index']);
Route::post('login/auth', ['as' => 'login.client.auth', 'uses' => 'LoginClientController@authUser']);
Route::get('logout', ['as' => 'login.client.logout', 'uses' => 'LoginClientController@logoutUser']);


/**
 * Register new user client
 */
Route::get('client/create', ['as' => 'client.create', 'uses' => 'ClientController@create']);
Route::post('client/store', ['as' => 'client.store', 'uses' => 'ClientController@store']);


/**
 * Settings User Client page
 */
Route::get('client', ['as' => 'client.index', 'middleware' => 'auth.client', 'uses' => 'ClientController@index']);


/*
|--------------------------------------------------------------------------
| Application Routes from ADMIN
|--------------------------------------------------------------------------
*/


/**
 * LOGIN PAGE ADMIN
 */
Route::get('admin/login', ['as' => 'login.admin.index', 'uses' => 'LoginAdminController@index']);
Route::post('admin/login/auth', ['as' => 'login.auth', 'uses' => 'LoginAdminController@authUser']);
Route::get('admin/logout', ['as' => 'login.admin.logout', 'uses' => 'LoginAdminController@logoutUser']);

/**
 * Routes from admin app (back)
 */
Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'where' => ['id' => '[0-9]+']], function () {

    /*
     * Categories Routes
     */
    Route::group(['prefix' => 'categories'], function () {
        Route::get('', ['as' => 'categories', 'uses' => 'CategoriesController@index']);
        Route::post('', ['as' => 'categories.store', 'uses' => 'CategoriesController@store']);
        Route::get('create', ['as' => 'categories.create', 'uses' => 'CategoriesController@create']);
        Route::get('{id}/destroy', ['as' => 'categories.destroy', 'uses' => 'CategoriesController@destroy']);
        Route::get('{id}/edit', ['as' => 'categories.edit', 'uses' => 'CategoriesController@edit']);
        Route::put('{id}/update', ['as' => 'categories.update', 'uses' => 'CategoriesController@update']);
    });


    /*
     * Products Routes
     */
    Route::group(['prefix' => 'products'], function () {
        Route::get('', ['as' => 'products', 'uses' => 'ProductsController@index']);
        Route::get('create', ['as' => 'products.create', 'uses' => 'ProductsController@create']);
        Route::post('', ['as' => 'products.store', 'uses' => 'ProductsController@store']);
        Route::get('{id}/destroy', ['as' => 'products.destroy', 'uses' => 'ProductsController@destroy']);
        Route::get('{id}/edit', ['as' => 'products.edit', 'uses' => 'ProductsController@edit']);
        Route::put('{id}/update', ['as' => 'products.update', 'uses' => 'ProductsController@update']);

        /*
         * Images Routes
         */
        Route::group(['prefix' => 'images'], function () {
            Route::get('{id}/product', ['as' => 'products.images', 'uses' => 'ProductsController@images']);
            Route::get('create/{id}/product', ['as' => 'products.images.create', 'uses' => 'ProductsController@createImage']);
            Route::post('store/{id}/product', ['as' => 'products.images.store', 'uses' => 'ProductsController@storeImage']);
            Route::get('destroy/{id}/image', ['as' => 'products.images.destroy', 'uses' => 'ProductsController@destroyImage']);
        });

    });


});


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
    ]);
