<?php namespace LojaVirtual;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{

    protected $fillable = ['product_id', 'file'];

    public function product()
    {
        return $this->belongsTo('LojaVirtual\Product');
    }

}
