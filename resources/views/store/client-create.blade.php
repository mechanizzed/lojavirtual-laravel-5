@extends('store.master')


@section('content')

    <section id="cart_items">
        <div class="container">

            <h2 class="title text-center">Faça seu registro</h2>

            <div class="col-lg-12">

                @if($errors->first("fail"))
                    {{ $errors->first("fail") }}
                @endif

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <div><i class="glyphicon glyphicon-alert"></i> {{ $error }}</div>
                        @endforeach
                    </div>
                @endif


                {!! Form::open(['route' => 'client.store', 'role' => 'form', 'method' => 'post']) !!}

                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Nome" value="{{ old('name') }}">
                </div>

                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" placeholder="password" name="password">
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" placeholder="confirmar password" name="password_confirmation">
                </div>

                <div class="form-group col-lg-3">
                    <button type="submit" class="btn btn-primary btn-block">Cadastrar</button>
                </div>
                {!! Form::close() !!}

            </div>


        </div>
    </section>


@stop