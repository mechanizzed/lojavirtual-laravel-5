@extends('store.master')


@section('content')

<section id="cart_items">
	<div class="container">

		<h2 class="title text-center">Olá {{ Auth::user()->name  }}</h2>

		<div class="col-lg-12">

			<h4>Meus Pedidos</h4>
			<hr>

			@foreach ($orders as $order)
			<div class="well">
				{{ $order->id }} - {{ $order->total }} - {{ $order->status }} <hr>
				@foreach ($order->items as $item)
				{{ $item->product->name }} <br>
				@endforeach
			</div>
			@endforeach

		</div>


	</div>
</section>


@stop