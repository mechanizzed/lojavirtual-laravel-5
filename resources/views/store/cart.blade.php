@extends('store.master')


@section('content')

    <section id="cart_items">
        <div class="container">

            @if($errors->first("fail"))
                {{ $errors->first("fail") }}
            @endif

            <div class="table-responsive cart_info">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr class="cart_menu">
                        <td>Item:</td>
                        <td>Nome:</td>
                        <td>Preço:</td>
                        <td>Quantidade:</td>
                        <td>Total</td>
                        <td></td>
                    </tr>
                    </thead>

                    <tbody>
                    @forelse($cart->all() as $k=>$item)
                        <tr>
                            <td class="col-lg-3">
                                <div class="col-lg-12 thumbnail">
                                    <img src="{{ asset('uploads/'.$item['file']) }}" width="100%" alt=""/>
                                </div>
                            </td>
                            <td>{{ $item['name'] }}</td>
                            <td>R$ {{ number_format($item['price'],2,",",".") }}</td>


                            <td class="col-lg-2">

                                <div class="col-lg-3">
                                    <a href="{{ route('cart.remove', ['id' => $k, 'qtd' => $item['qtd']]) }}"
                                       class="btn btn-xs btn-danger pull-right" style="margin-top: 20px">
                                        <i class="glyphicon glyphicon-minus"></i>
                                    </a>
                                </div>

                                <div class="col-lg-6">
                                    <input type="text" name="qtd" value="{{ $item['qtd'] }}"
                                           class="form-control text-center disabled pull-left" disabled>
                                </div>

                                <div class="col-lg-3">
                                    <a href="{{ route('cart.add', ['id' => $k]) }}"
                                       class="btn btn-xs btn-success pull-right" style="margin-top: 20px">
                                        <i class="glyphicon glyphicon-plus"></i>
                                    </a>
                                </div>


                            </td>

                            <td>Total: R$ {{ number_format($item['price'] *  $item['qtd'],2,",",".")  }}</td>
                            <td><a href="{{ route('cart.destroy', ['id' => $k]) }}" class="btn btn-sm btn-danger">Deletar</a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="container" colspan="6">Nenhum produto adicionado</td>
                        </tr>
                    @endforelse

                    <tr class="bg-primary">
                        <td colspan="6">
                            <div class="text-right col-md-12">
                                <h3 class="text-success">Total a pagar:
                                    R$ {{ number_format($cart->getTotal(),2,",",".") }}</h3></div>
                            <div class="text-right col-md-12">
                                <a href="{{ route('checkout.place') }}" class="btn btn-success">Fechar Pedido</a>
                            </div>
                        </td>
                    </tr>

                    </tbody>

                </table>
            </div>

        </div>
    </section>


@stop