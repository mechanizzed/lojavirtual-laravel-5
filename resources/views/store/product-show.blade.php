@extends('store.master')

@section('slider_home')
    @include('store.partials.slider_home')
@stop


@section('category')
    @include('store.partials.category')
@stop

@section('content')



    <div class="col-sm-9 padding-right">
        <div class="product-details"><!--product-details-->
            <div class="col-sm-5">
                <div class="thumbnail">
                    @if(count($product->images))
                        <img src="{{ asset('uploads/'.$product->images->first()->file) }}" width="100%" alt=""/>
                    @else
                        <img src="{{ asset('store/images/no-img.jpg') }}" alt="" width="100%"/>
                    @endif
                </div>

                <div id="similar-product" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <a href="#"><img src="{{ asset('store/images/no-img.jpg') }}" alt="" width="80"></a>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-sm-7">


                <strong>Categoria:</strong>
                <small>{{ $product->category->name }}</small>
                <br/>
                <strong>Tags relacionadas:</strong>
                @foreach($product->tags as $tag)
                    <a href="{{ route('tag', ['id' => $tag->id]) }}" class="btn btn-xs btn-warning">{{ $tag->name }}</a>
                @endforeach

                <h1>{{ $product->name }}</h1>

                <p>{{ $product->description }}</p>
                <span>R$ {{ $product->price }}</span>
                <a href="{{ route('cart.add', ['id' => $product->id]) }}" class="btn btn-success cart">
                    <i class="fa fa-shopping-cart"></i>
                    Adicionar no Carrinho
                </a>


            </div>
        </div>
        <!--/product-details-->
    </div>


@stop