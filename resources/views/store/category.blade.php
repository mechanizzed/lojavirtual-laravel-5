@extends('store.master')

@section('slider_home')
    @include('store.partials.slider_home')
@stop


@section('category')
    @include('store.partials.category')
@stop

@section('content')


    <div class="col-sm-9 padding-right">
        <div class="features_items"><!--categories-->
            <h2 class="title text-center">{{ $category->name }}</h2>

            @include('store.partials.products', ['products' => $products])

        </div>
        <!--categories-->


    </div>


@stop