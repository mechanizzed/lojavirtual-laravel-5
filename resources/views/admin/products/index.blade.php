@extends('admin.app')
@section('content')


    <section class="content-header">
        <h1>
            Produtos
            <small>- Produtos cadastrados no sistema</small>
        </h1>
    </section>

    <div class="content">


        <div class="panel panel-default">
            <div class="panel-body">


                <div class="box">
                    <div class="box-header">
                        <div class="margin">
                            <a href="{{ route('products.create') }}" class="btn btn-success"><i
                                        class="glyphicon glyphicon-plus"></i> Adicionar
                                Produtos</a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Descrição</th>
                                <th>Tags</th>
                                <th>Preço</th>
                                <th>Categoria</th>
                                <th>Ação</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($products as $product)
                                <tr>
                                    <td>{{ $product->id }}</td>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->description }}</td>
                                    <td>
                                        @foreach($product->tags as $tag)
                                            <div class="btn btn-xs btn-default">{{ $tag->name }}</div>
                                        @endforeach
                                    </td>
                                    <td>{{ $product->price }}</td>
                                    <td>{{ $product->category->name }}</td>
                                    <td>

                                        <a href="{{ route('products.images', $product->id) }}">
                                            <button class="btn btn-sm btn-primary"><i
                                                        class="glyphicon glyphicon-camera"></i> Imagens
                                            </button>
                                        </a>

                                        <a href="{{ route('products.edit', ['id' => $product->id]) }}">
                                            <button class="btn btn-sm btn-warning"><i
                                                        class="glyphicon glyphicon-edit"></i> Editar
                                            </button>
                                        </a>
                                        <a href="{{ route('products.destroy', ['id' => $product->id]) }}">
                                            <button class="btn btn-sm btn-danger"><i
                                                        class="glyphicon glyphicon-remove"></i> Deletar
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>

                        {!! $products->render() !!}

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


            </div>
        </div>


    </div>


@stop